package asfastasyoucan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class AsFastAsYouCanScore extends JFrame implements ActionListener {
    private JLabel result;
    private JButton playAgain,backtomenu;
    private int currentPoints;
    private String record,tmpResult;
    AsFastAsYouCanScore() throws SQLException {
        super("");
        currentPoints=AsFastAsYouCanGame.getPoints();
        try {
            record=Login.getTmpV();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(currentPoints>Integer.valueOf(record)){
            tmpResult="<html> NOWY REKORD!!! <br/>"+currentPoints+"</html>";
            AsFastAsYouCanGame.setPoints(0);
            Connection con=Connectioner.connect();
            Statement stmt=con.createStatement();
            stmt.executeUpdate("UPDATE users set points="+currentPoints+" where ID="+Login.getTmpID()+"");
        }
        else {
            tmpResult = "<html> TWOJ WYNIK TO <br/>" + currentPoints + "</html>";
            AsFastAsYouCanGame.setPoints(0);
        }
        result=new JLabel(tmpResult);
        result.setBounds(100,100,250,70);
        result.setFont(new Font("",Font.BOLD,30));
        backtomenu=new JButton("MENU");
        backtomenu.setBounds(250,200,100,35);
        backtomenu.addActionListener(this);
        playAgain=new JButton("PLAY");
        playAgain.setBounds(100,200,100,35);
        playAgain.setFont(new Font("",Font.BOLD,30));
        playAgain.addActionListener(this);
        setSize(400,300);
        setLayout(null);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        setVisible(true);
        Image icon=Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.png"));
        setIconImage(icon);
        add(result);add(playAgain);add(backtomenu);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(playAgain))
        {
            try {
                new AsFastAsYouCanGame();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            setVisible(false);
        }
        if(e.getSource().equals(backtomenu))
        {
            try {
                new AsFastAsYouCanMenu();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            setVisible(false);
        }
    }
}
