package asfastasyoucan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public abstract class AsFastAsYouCanInterface extends JFrame implements ActionListener, KeyListener, AsFastYouCanActionPerformed {
    int width,height;
    public AsFastAsYouCanInterface(int width,int height){
        this.width=width;
        this.height=height;
        setSize(width,height);
        setLayout(null);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);
        setVisible(true);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("fast.png"));
        setIconImage(icon);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    @Override
    public void actionPerformed(ActionEvent e, ActionPerformedClass[]actionPerformedClasses,AsFastAsYouCanInterface asFastAsYouCanInterface){
        for (ActionPerformedClass action:
             actionPerformedClasses) {
        if (e.getSource().equals(action.getObj1())) {
            action.setObj2(new Object());
            asFastAsYouCanInterface.setVisible(false);
        }
        }
    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

}
