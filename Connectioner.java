package asfastasyoucan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

class Connectioner {
    static Connection connect(){
        Connection con=null;
        try {
            Properties connectionProps = new Properties();
            connectionProps.put("user", "root");
            connectionProps.put("password", "");
            connectionProps.put("serverTimezone", "Europe/Warsaw");
            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/unlockerdatabase"
                    , connectionProps);
        }catch(Exception e){ System.out.println(e);
        }
        return con;
    }
}
