package asfastasyoucan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Random;

class NumbersButton extends JButton implements ActionListener {
    private String cyfra;
    private JLabel textLabel,icon;
    private Random random=new Random();
    NumbersButton(String cyfra,JLabel icon,int event, JLabel textLabel){
        setFont(new Font("",Font.BOLD,35));
        this.textLabel=textLabel;
        this.cyfra = cyfra;
        this.icon=icon;
        setFocusPainted(false);
        registerKeyboardAction(getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(event, 0, false),
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        registerKeyboardAction(getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(event, 0, true),
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        addActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
            textLabel.setText(textLabel.getText().concat(cyfra));
            icon.setBounds(random.nextInt(378),random.nextInt(131)+300,50,50);
            AsFastAsYouCanGame.setTimer(icon);
        }
    }

