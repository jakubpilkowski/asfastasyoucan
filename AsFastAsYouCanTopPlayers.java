package asfastasyoucan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class AsFastAsYouCanTopPlayers extends AsFastAsYouCanInterface {
    private JButton exit;
    private JLabel title,yourPoints,yourPlace;
    private JLabel topPlayers;
    private JLabel yourName;
    AsFastAsYouCanTopPlayers() throws SQLException {
        super(350,400);
        title=new JLabel(new ImageIcon(getClass().getResource("rsz_fast.png")));
        title.setBounds(150,10,50,31);
        Font fontRecords=new Font("",Font.BOLD,20);
        //topPlayers
        topPlayers=new JLabel("Top Players");
        topPlayers.setBounds(115,50,120,25);
        topPlayers.setFont(fontRecords);
        //yourPlace
        yourPlace=new JLabel("<html>Place<br/>");
        yourPlace.setBounds(50,80,70,200);
        yourPlace.setFont(fontRecords);
        //yourName
        yourName=new JLabel("<html>Nick<br/>");
        yourName.setBounds(120,80,100,200);
        yourName.setFont(fontRecords);
        //yourPoints
        yourPoints=new JLabel("<html>Points<br/>");
        yourPoints.setBounds(220,80,80,200);
        yourPoints.setFont(fontRecords);
        //Exit
        exit=new JButton("Back");
        exit.setBounds(25,300,100,40);
        exit.addKeyListener(this);
        exit.setForeground(Color.white);
        exit.setBackground(Color.BLACK);
        //Wczytanie
        Connection con=Connectioner.connect();
        Statement stmt=con.createStatement();
        ResultSet resultSet=stmt.executeQuery("SELECT UserName,points FROM users ORDER BY points DESC LIMIT 5 ");
        int i=1;
        while (resultSet.next()) {
            yourPlace.setText(yourPlace.getText().concat("<html>"+i+"    <br/>"));
            yourName.setText(yourName.getText().concat("<html>"+ resultSet.getString(1) + " <br/>"));
            yourPoints.setText(yourPoints.getText().concat("<html>"+resultSet.getInt(2)+"<br/>"));
            i++;
        }
        yourPlace.setText(yourPlace.getText().concat("</html>"));
        yourName.setText(yourName.getText().concat("</html>"));
        yourPoints.setText(yourPoints.getText().concat("</html>"));
        //Frame
        add(yourName);add(exit);add(topPlayers);add(title);add(yourPoints);add(yourPlace);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getSource().equals(exit)){
            if(e.getKeyCode()==KeyEvent.VK_ESCAPE){
                try {
                    new AsFastAsYouCanMenu();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            setVisible(false);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
