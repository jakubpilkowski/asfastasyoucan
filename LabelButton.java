package asfastasyoucan;

import javax.swing.*;
import java.awt.*;

public class LabelButton extends JLabel  {
    LabelButton(ImageIcon icon){
        super(icon);
        setVisible(false);
    }
    LabelButton(ImageIcon icon,int x,int y,int length, int height){
        super(icon);
        setBounds(x,y,length,height);
    }
    LabelButton(ImageIcon icon,int x,int y,int length, int height,int fontsize){
        super(icon);
        setBounds(x,y,length,height);
        setFont(new Font("",Font.BOLD,fontsize));
    }
    LabelButton(String tekst,int x,int y,int length, int height){
        super(tekst);
        setBounds(x,y,length,height);
    }
    LabelButton(String tekst,int x,int y,int length, int height,int fontsize){
        super(tekst);
        setBounds(x,y,length,height);
        setFont(new Font("",Font.BOLD,fontsize));
    }
    LabelButton(String tekst,int x,int y,int length, int height,int fontsize,Color color){
        super(tekst);
        setBounds(x,y,length,height);
        setFont(new Font("",Font.BOLD,fontsize));
        setForeground(color);
    }
}
