package asfastasyoucan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.*;

import static java.awt.Font.BOLD;

public class Register extends AsFastAsYouCanInterface{
    private JLabel title,titleCD,usernameLabel,passwordLabel,infoLabel,warningLabel;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JButton login;
    boolean czyJest=false;
    Register(){
        super(350,400);
        //title
        title=new JLabel(new ImageIcon(getClass().getResource("rsz_fast.png")));
        title.setBounds(150,10,50,31);
        //titleCD
        titleCD=new JLabel(" As Fast As You Can");
        titleCD.setBounds(25,50,300,35);
        titleCD.setFont(new Font("", BOLD,31));
        //usernameLabel
        usernameLabel=new JLabel("Username:");
        usernameLabel.setBounds(50,120,120,25);
        usernameLabel.setFont(new Font("",Font.BOLD,20));
        //passwordLabel
        passwordLabel=new JLabel("Password:");
        passwordLabel.setBounds(50,160,120,25);
        passwordLabel.setFont(new Font("",Font.BOLD,20));
        //warningLabel
        warningLabel=new JLabel("");
        warningLabel.setBounds(45,310,260,18);
        warningLabel.setFont(new Font("",Font.BOLD,15));
        warningLabel.setForeground(Color.red);
        //usernameField
        usernameField=new JTextField();
        usernameField.setBounds(185,120,120,25);
        usernameField.requestFocus();
        //passwordField
        passwordField=new JPasswordField();
        passwordField.setBounds(185,160,120,25);
        //infoLabel
        infoLabel=new JLabel("<html>Your username and password need to <br/> have between 4 and 16 signs!</html>");
        infoLabel.setBounds(50,210,250,28);
        infoLabel.setFont(new Font("", BOLD,11));
        //login
        login=new JButton("Register");
        login.setBounds(50,250,250,30);
        login.addActionListener(this);
        login.registerKeyboardAction(login.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        login.registerKeyboardAction(login.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        login.setForeground(Color.white);
        login.setBackground(Color.BLACK);
        //frame
        add(title);add(titleCD);add(infoLabel);
        add(usernameLabel);add(passwordLabel);add(warningLabel);add(usernameField);add(passwordField);
        add(login);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(login)) {
            try {
                zarejestruj();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
    void zarejestruj() throws SQLException {
            Connection con=Connectioner.connect();
            Statement stmt=con.createStatement();
            String username=usernameField.getText();
            char[] passwordtmp=passwordField.getPassword();
            StringBuilder password = new StringBuilder();
            for (char tmp:passwordtmp) {
                password.append(tmp);
            }
            if(username.length()<4||password.length()<4||username.length()>16||password.length()>16) {
                warningLabel.setText("Too short username or password!!!");
                czyJest=true;
            }
            ResultSet rs=stmt.executeQuery("SELECT UserName,Password from users");
            while (rs.next()) {
                if (username.equals(rs.getString(1))) {
                    warningLabel.setText("Login or password already exists!!!");
                     czyJest=true;
                     break;
                }
            }
            if(!czyJest) {
                    stmt.executeUpdate("INSERT INTO users (ID,UserName,Password,points) values (NULL,'" + username + "','" + password.toString() + "',0)");
                    stmt.executeUpdate("INSERT INTO settings(id,users_id,dlugosc,czas,seconds) values (NULL ,(SELECT ID from users WHERE UserName='" + username + "' AND Password='" + password.toString() + "'),1,1,0)");
            setVisible(false);
            new Login();
            con.close();
            }
    }
}

