package asfastasyoucan;

import sun.rmi.runtime.Log;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;

import static com.sun.glass.ui.Cursor.setVisible;

public class AsFastAsYouCanMenu extends AsFastAsYouCanInterface {
    private JLabel yourName, title, titleCD, yourRecordCD;
    private static JLabel yourRecord;
    private MenuButtons playButton, settingsButton, rankingButton, logoutButton;
    private AsFastAsYouCanMenu asFastAsYouCanMenu;
    private AsFastAsYouCanGame asFastAsYouCanGame;
    private AsFastAsYouCanSettings asFastAsYouCanSettings;
    private AsFastAsYouCanTopPlayers asFastAsYouCanTopPlayers;
    private Login login;
    private ActionPerformedClass[] actionPerformedClasses;
    AsFastAsYouCanMenu() throws SQLException {
        super(438, 500);
        actionPerformedClasses=new ActionPerformedClass[4];
        actionPerformedClasses[0]=new ActionPerformedClass(playButton,asFastAsYouCanGame);
        actionPerformedClasses[1]=new ActionPerformedClass(settingsButton,asFastAsYouCanSettings);
        actionPerformedClasses[2]=new ActionPerformedClass(rankingButton,asFastAsYouCanTopPlayers);
        actionPerformedClasses[3]=new ActionPerformedClass(logoutButton,login);
        System.out.println(actionPerformedClasses[0].getObj1());
        System.out.println(actionPerformedClasses[0].getObj2());
        //title
        title = new JLabel(new ImageIcon(getClass().getResource("rsz_fast.png")));
        title.setBounds(194, 50, 50, 31);
        //titleCD
        titleCD = new JLabel(" As Fast As You Can");
        titleCD.setBounds(69, 90, 300, 35);
        titleCD.setFont(new Font("", Font.BOLD, 31));
        //yourName
        yourName = new JLabel(Login.getTmpU());
        yourName.setBounds(50, 0, 250, 35);
        yourName.setFont(new Font("", Font.BOLD, 31));
        //yourRecord
        yourRecord = new JLabel(new ImageIcon(getClass().getResource("rsz_fast.png")));
        yourRecord.setBounds(273, 0, 35, 31);
        //yourRecordCD
        yourRecordCD = new JLabel(": " + Login.getTmpV());
        yourRecordCD.setBounds(308, 0, 80, 31);
        yourRecordCD.setFont(new Font("", Font.BOLD, 30));
        //PlayButton
        playButton = new MenuButtons("Play", 119, 140, 200, 50);
        playButton.requestFocus();
        playButton.addKeyListener(this);
        playButton.addActionListener(this);
        //SettingsButton
        settingsButton = new MenuButtons("Settings", 119, 215, 200, 50);
        settingsButton.addKeyListener(this);
        settingsButton.addActionListener(this);
        //TopPlayersButton
        rankingButton = new MenuButtons("Ranking", 119, 290, 200, 50);
        rankingButton.addKeyListener(this);
        rankingButton.addActionListener(this);
        //LogoutButton
        logoutButton = new MenuButtons("Log Out", 119, 365, 200, 50);
        logoutButton.addKeyListener(this);
        logoutButton.addActionListener(this);
        //Frame
        add(yourName);
        add(yourRecord);
        add(yourRecordCD);
        add(playButton);
        add(settingsButton);
        add(rankingButton);
        add(logoutButton);
        add(title);
        add(titleCD);
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource().equals(playButton)) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                try {
                    new AsFastAsYouCanGame();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                setVisible(false);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP)
                logoutButton.requestFocus();
            if (e.getKeyCode() == KeyEvent.VK_DOWN)
                settingsButton.requestFocus();
        }
        if (e.getSource().equals(settingsButton)) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                try {
                    new AsFastAsYouCanSettings();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                setVisible(false);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP)
                playButton.requestFocus();
            if (e.getKeyCode() == KeyEvent.VK_DOWN)
                rankingButton.requestFocus();
        }
        if (e.getSource().equals(rankingButton)) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                try {
                    new AsFastAsYouCanTopPlayers();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                setVisible(false);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP)
                settingsButton.requestFocus();
            if (e.getKeyCode() == KeyEvent.VK_DOWN)
                logoutButton.requestFocus();
        }
        if (e.getSource().equals(logoutButton)) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                try {
                    new Login();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                setVisible(false);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP)
                rankingButton.requestFocus();
            if (e.getKeyCode() == KeyEvent.VK_DOWN)
                playButton.requestFocus();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(null,actionPerformedClasses,asFastAsYouCanMenu);
    }
}