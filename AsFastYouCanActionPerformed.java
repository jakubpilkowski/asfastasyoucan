package asfastasyoucan;

import java.awt.*;
import java.awt.event.ActionEvent;

public interface AsFastYouCanActionPerformed {
    void actionPerformed(ActionEvent e, ActionPerformedClass[] actionPerformedClasses, AsFastAsYouCanInterface asFastAsYouCanInterface);
}
