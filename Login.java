package asfastasyoucan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;

public class Login extends AsFastAsYouCanInterface {
    private LabelButton title,titleCD,register,usernameLabel,passwordLabel,warningLabel;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JButton login;
    static String tmpID,tmpU,tmpP,tmpV;
    private static Connection con=Connectioner.connect();
    private static Statement stmt;
    static {
        try {
            stmt = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    Login() throws SQLException{
        super(350,400);
        //title
        title=new LabelButton(new ImageIcon(getClass().getResource("rsz_fast.png")),150,10,50,31);
        //titleCD
        titleCD=new LabelButton(" As Fast As You Can",25,50,300,35,31);
        //usernameLabel
        usernameLabel=new LabelButton("Username:",50,120,120,25,20);
        //passwordLabel
        passwordLabel=new LabelButton("Password:",50,160,120,25,20);
        //usernameField
        usernameField=new JTextField();
        usernameField.setBounds(185,120,120,25);
        //passwordField
        passwordField=new JPasswordField();
        passwordField.setBounds(185,160,120,25);
        //warningLabel
        warningLabel=new LabelButton("",45,310,260,18,15,Color.RED);
        //register
        register=new LabelButton("<HTML><U>Click here to register!!!</U></HTML>",50,210,130,14,11);
        register.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        register.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
                setVisible(false);
                new Register();
            }
        });
        //login
        login=new JButton("Login");
        login.setBounds(50,250,250,30);
        login.addActionListener(this);
        login.setBackground(Color.BLACK);
        login.setForeground(Color.white);
        login.registerKeyboardAction(login.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        login.registerKeyboardAction(login.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        //frame
        add(title);add(titleCD);
        add(usernameLabel);add(passwordLabel);add(warningLabel);
        add(usernameField);add(passwordField);
        add(register);add(login);
    }
    private void zaloguj() throws SQLException {
        boolean jestTakiUser=false;
        boolean jestTakieHaslo=false;
        tmpU=usernameField.getText();
        tmpP=String.valueOf(passwordField.getPassword());
        ArrayList<String>usernames = new ArrayList<>();
        ArrayList<String>passwords=new ArrayList<>();
        ResultSet ru = stmt.executeQuery("SELECT UserName,Password FROM users ");
        while(ru.next()){
            usernames.add(ru.getString(1));
            passwords.add(ru.getString(2));
        }
        for (String tmp:usernames)
            if (tmp.equals(tmpU)) {
                jestTakiUser = true;
                break;
            }
        for (String tmp:passwords)
            if (tmp.equals(tmpP)) {
                jestTakieHaslo = true;
                break;
            }
        if(jestTakiUser&&jestTakieHaslo) {
                setVisible(false);
                new AsFastAsYouCanMenu();
        }
        else
            warningLabel.setText("Username or password doesnt exist!");
    }

    static String getTmpID() throws SQLException {
        ResultSet ru=stmt.executeQuery("SELECT ID FROM users where UserName='"+getTmpU()+"'and Password='"+getTmpP()+"'");
        ru.next();
        return ru.getString(1);
    }

    static String getTmpU() {
        return tmpU;
    }

    private static String getTmpP() {
        return tmpP;
    }
    static String getTmpV() throws SQLException {
        ResultSet ru=stmt.executeQuery("SELECT points FROM users where UserName='"+getTmpU()+"'and Password='"+getTmpP()+"'");
        ru.next();
        return ru.getString(1);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(login)) {
            try {
                zaloguj();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
    public static void main(String[] args) throws SQLException {
        new Login();
    }
}
