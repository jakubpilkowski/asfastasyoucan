package asfastasyoucan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

class MenuButtons extends JButton {
    MenuButtons( String tekst, int x, int y, int width, int length) {
        super(tekst);
        setBackground(Color.BLACK);
        setForeground(Color.WHITE);
        setFont(new Font("", Font.BOLD, 40));
        setBounds(x, y, width, length);
    }
    MenuButtons(String tekst, int event){
        super(tekst);
        registerKeyboardAction(getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(event, 0, false),
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        registerKeyboardAction(getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(event, 0, true),
                JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
}
