package asfastasyoucan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AsFastAsYouCanSettings extends AsFastAsYouCanInterface {
    private JButton Save, Exit;
    private JLabel title,dlugoscHasla,nonstandardTimeLabel,settings, czasNaZgadniecie;
    private static JComboBox ustawDlugosc, ustawCzas,ustawSekundy;
    private static Connection con = Connectioner.connect();
    private static Statement stmt;

    static {
        try {
            stmt = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    AsFastAsYouCanSettings() throws SQLException {
        super(350,400);
        Font ButtonsFont = new Font("", Font.BOLD, 20);
        //Title
        title=new JLabel(new ImageIcon(getClass().getResource("rsz_fast.png")));
        title.setBounds(150,10,50,31);
        //settings
        settings=new JLabel("Settings");
        settings.setBounds(125,50,100,25);
        settings.setFont(ButtonsFont);
        //ButtonsFont
        ResultSet rs=stmt.executeQuery("SELECT dlugosc,czas,seconds from settings where users_id="+Login.getTmpID()+"");
        rs.next();
        int x=rs.getInt(1);
        int y=rs.getInt(2);
        int z=rs.getInt(3);
        //DlugoscHasla
        dlugoscHasla = new JLabel("Length: ");
        dlugoscHasla.setBounds(95, 100, 100, 25);
        dlugoscHasla.setFont(new Font("", Font.BOLD, 20));
        //ustawDlugosc
        String[] dlugosci = {"1", "2", "3", "4", "5"};
        ustawDlugosc = new JComboBox(dlugosci);
        ustawDlugosc.setSelectedItem(dlugosci[x-1]);
        ustawDlugosc.setBounds(205, 100, 40, 25);
        ustawDlugosc.setForeground(Color.WHITE);
        ustawDlugosc.setBackground(Color.BLACK);
        //czasNaZgadniecie
        czasNaZgadniecie = new JLabel("Minutes: ");
        czasNaZgadniecie.setBounds(95, 150, 100, 25);
        czasNaZgadniecie.setFont(new Font("", Font.BOLD, 20));
        //ustawCzas
        String[] czasy = new String[60];
        for (int i = 0; i <60 ; i++) czasy[i] = String.valueOf(i);
        ustawCzas = new JComboBox(czasy);
        ustawCzas.setSelectedItem(czasy[y]);
        ustawCzas.setBounds(205, 150, 40, 25);
        ustawCzas.setForeground(Color.white);
        ustawCzas.setBackground(Color.BLACK);
        //nonstandardTimeLabel
        nonstandardTimeLabel=new JLabel("Seconds:");
        nonstandardTimeLabel.setBounds(95,200,100,25);
        nonstandardTimeLabel.setFont(new Font("",Font.BOLD,20));
        //ustawSekundy
        ustawSekundy=new JComboBox(czasy);
        ustawSekundy.setSelectedItem(czasy[z]);
        ustawSekundy.setBounds(205,200,40,25);
        ustawSekundy.setForeground(Color.white);
        ustawSekundy.setBackground(Color.BLACK);
        //ExitButton
        Exit = new JButton("Back");
        Exit.setBounds(40,300 , 85, 30);
        Exit.setFont(ButtonsFont);
        Exit.setForeground(Color.WHITE);
        Exit.setBackground(Color.BLACK);
        Exit.addKeyListener(this);
        //SaveButton
        Save = new JButton("Save");
        Save.setBounds(225, 300, 85, 30);
        Save.setFont(ButtonsFont);
        Save.setForeground(Color.WHITE);
        Save.setBackground(Color.BLACK);
        Save.addKeyListener(this);
        add(Exit);
        add(Save);add(title);
        add(ustawDlugosc);
        add(ustawCzas);add(settings);
        add(ustawSekundy);add(nonstandardTimeLabel);
        add(dlugoscHasla);
        add(czasNaZgadniecie);
    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource().equals(Save)) {
            if(e.getKeyCode()==KeyEvent.VK_LEFT||e.getKeyCode()==KeyEvent.VK_RIGHT)
                Exit.requestFocus();
            if(e.getKeyCode()==KeyEvent.VK_ENTER){
            try {
                int tmpD= Integer.valueOf(String.valueOf(ustawDlugosc.getItemAt(ustawDlugosc.getSelectedIndex())));
                int tmpC= Integer.valueOf(String.valueOf(ustawCzas.getItemAt(ustawCzas.getSelectedIndex())));
                int tmpS=Integer.valueOf(String.valueOf(ustawSekundy.getItemAt(ustawSekundy.getSelectedIndex())));
                System.out.println(Login.getTmpID());
                stmt.executeUpdate(
                        "UPDATE settings set dlugosc="+ tmpD +", czas="+tmpC+", seconds="+tmpS+" " +
                                "WHERE users_id=" + Login.getTmpID() + "");
                setVisible(false);
                new AsFastAsYouCanMenu();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }}
        }
        if (e.getSource().equals(Exit)) {
            if(e.getKeyCode()==KeyEvent.VK_RIGHT||e.getKeyCode()==KeyEvent.VK_LEFT)
                Save.requestFocus();
            if(e.getKeyCode()==KeyEvent.VK_ENTER){
                setVisible(false);

                try {
                new AsFastAsYouCanMenu();
                } catch (SQLException e1) {
                e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
