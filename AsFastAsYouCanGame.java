package asfastasyoucan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class AsFastAsYouCanGame extends AsFastAsYouCanInterface {
    private long minutes, seconds, miliseconds = 0;
    private int length;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private boolean stopStopWatch;
    static JLabel yourRecordCD, yourPoints, yourRecord, score;
    private JLabel minutesAndSecondsLabel, milisecondsLabel, textLabel, tmpShow, incorrect, correct,clockicon;
    private LabelButton buttonicon,buttonicon1,buttonicon2,buttonicon3,buttonicon4,buttonicon5,buttonicon6,buttonicon7,buttonicon8,buttonicon9;
    private static long start;
    private int number;
    private static int points = 0;
    private NumbersButton n0, n1, n2, n3, n4, n5, n6, n7, n8, n9;
    private JButton clear, backspace, check;

    AsFastAsYouCanGame() throws SQLException {
        super(438,500);
        Font labelFont = new Font("", Font.BOLD, 40);
        Font milisecondsFont = new Font("", Font.BOLD, 30);
        Connection con = Connectioner.connect();
        Statement stmt = con.createStatement();
        ResultSet ru = stmt.executeQuery("SELECT dlugosc,czas,seconds FROM settings where users_id='" + Login.getTmpID() + "'");
        ru.next();
        length = ru.getInt(1);
        minutes = ru.getInt(2);
        seconds = ru.getInt(3);
        //incorrect
        incorrect = new JLabel(new ImageIcon(getClass().getResource("incorrect50x50.png")));
        incorrect.setBounds(195, 250, 50, 50);
        incorrect.setVisible(false);
        //correct
        correct = new JLabel(new ImageIcon(getClass().getResource("correct50x50.png")));
        correct.setBounds(195, 250, 50, 50);
        correct.setVisible(false);
        //clockicon
        clockicon=new JLabel(new ImageIcon(getClass().getResource("iconfinder_icon-clock_2867879.png")));
        clockicon.setBounds(104,100,40,40);
        //LABEL MINUTES_AND_SECONDS_LABEL
        minutesAndSecondsLabel = new JLabel(String.format("%02d:%02d", minutes, seconds));
        minutesAndSecondsLabel.setBounds(144, 100, 110, 40);
        minutesAndSecondsLabel.setFont(labelFont);
        //LABEL MILISECONDS_LABEL
        milisecondsLabel = new JLabel(String.format(":%03d", miliseconds));
        milisecondsLabel.setBounds(244, 107, 90, 30);
        milisecondsLabel.setFont(milisecondsFont);
        //yourRecord
        yourPoints = new JLabel(new ImageIcon(getClass().getResource("rsz_fast.png")));
        yourPoints.setBounds(200, 0, 35, 31);
        //score
        score = new JLabel(": " + points);
        score.setBounds(235, 0, 80, 31);
        score.setFont(new Font("", Font.BOLD, 30));
        //yourRecordCD
        yourRecordCD = new JLabel(new ImageIcon(getClass().getResource("iconfinder_school_object_study_student-5_2038397.png")));
        yourRecordCD.setBounds(315, 0, 35, 35);
        //yourRecord
        yourRecord = new JLabel(": " + Login.getTmpV());
        yourRecord.setBounds(350, 0, 80, 35);
        yourRecord.setFont(new Font("", Font.BOLD, 30));
        //tmpShow
        tmpShow = new JLabel("");
        tmpShow.setBounds(219 - (length * 12), 150, 25 * length, 50);
        tmpShow.setFont(new Font("", Font.BOLD, 45));
        //textLabel
        textLabel = new JLabel("");
        textLabel.setBounds(207 - (length - 1) * 12, 200, 25 * length, 50);
        textLabel.setFont(new Font("", Font.BOLD, 45));
        //icon
        buttonicon=new LabelButton(new ImageIcon(getClass().getResource("iconfinder_number_0_blue_1553042.png")));
        buttonicon1=new LabelButton(new ImageIcon(getClass().getResource("iconfinder_number_1_blue_1553030.png")));
        buttonicon2=new LabelButton(new ImageIcon(getClass().getResource("iconfinder_number_2_blue_1553043.png")));
        buttonicon3=new LabelButton(new ImageIcon(getClass().getResource("iconfinder_number_3_blue_1553079.png")));
        buttonicon4=new LabelButton(new ImageIcon(getClass().getResource("iconfinder_number_4_blue_1553097.png")));
        buttonicon5=new LabelButton(new ImageIcon(getClass().getResource("iconfinder_number_5_blue_1553045.png")));
        buttonicon6=new LabelButton(new ImageIcon(getClass().getResource("iconfinder_number_6_blue_1553053.png")));
        buttonicon7=new LabelButton(new ImageIcon(getClass().getResource("iconfinder_number_7_blue_1553034.png")));
        buttonicon8=new LabelButton(new ImageIcon(getClass().getResource("iconfinder_number_8_blue_1553070.png")));
        buttonicon9=new LabelButton(new ImageIcon(getClass().getResource("iconfinder_number_9_blue_1553064.png")));
        //buttons
        n0 = new NumbersButton("0", buttonicon, KeyEvent.VK_0, textLabel);
        n1 = new NumbersButton("1", buttonicon1,KeyEvent.VK_1,textLabel);
        n2 = new NumbersButton("2", buttonicon2, KeyEvent.VK_2, textLabel);
        n3 = new NumbersButton("3", buttonicon3, KeyEvent.VK_3, textLabel);
        n4 = new NumbersButton("4", buttonicon4, KeyEvent.VK_4, textLabel);
        n5 = new NumbersButton("5", buttonicon5, KeyEvent.VK_5, textLabel);
        n6 = new NumbersButton("6", buttonicon6, KeyEvent.VK_6, textLabel);
        n7 = new NumbersButton("7", buttonicon7, KeyEvent.VK_7, textLabel);
        n8 = new NumbersButton("8", buttonicon8, KeyEvent.VK_8, textLabel);
        n9 = new NumbersButton("9", buttonicon9, KeyEvent.VK_9, textLabel);
        //BUTTON CLEAR
        clear = new MenuButtons("", KeyEvent.VK_C);
        clear.addActionListener(this);
        //BUTTON BACKSPACE
        backspace = new MenuButtons("", KeyEvent.VK_BACK_SPACE);
        backspace.addActionListener(this);
        //Check
        check = new MenuButtons("", KeyEvent.VK_ENTER);
        check.addActionListener(this);
        //Frame
        add(score);
        add(yourPoints);
        add(textLabel);
        add(clockicon);
        add(buttonicon);add(buttonicon1);add(buttonicon2);add(buttonicon3);add(buttonicon4);add(buttonicon5);
        add(buttonicon6);add(buttonicon7);add(buttonicon8);add(buttonicon9);
        add(yourRecord);
        add(yourRecordCD);
        add(incorrect);
        add(correct);
        add(tmpShow);
        add(n0);
        add(n1);
        add(n2);
        add(n3);
        add(n4);
        add(n5);
        add(n6);
        add(n7);
        add(n8);
        add(n9);
        add(check);
        add(clear);
        add(backspace);
        add(minutesAndSecondsLabel);
        add(milisecondsLabel);
        stopStopWatch = false;
        start = System.currentTimeMillis();
        stopwatch();
        random();
        tmpShow.setText(String.valueOf(number));
    }

    private void stopwatch() {
        final Runnable runnable = () -> {
            boolean pierwszyRaz = false;
            while (!stopStopWatch) {
                if (!pierwszyRaz) {
                    if (seconds == 0)
                        seconds = 59;
                    else
                        seconds--;
                    miliseconds = 999;
                    pierwszyRaz = true;
                }
                miliseconds = 999 - (System.currentTimeMillis() - start);
                if (miliseconds < 0) {
                    seconds--;
                    miliseconds = 999;
                    start += 1000;
                }
                if (seconds == 0 && miliseconds < 0) {
                    minutes--;
                    seconds = 60;
                }
                if (seconds == -1) {
                    milisecondsLabel.setText(String.format(":%03d", 0));
                    minutesAndSecondsLabel.setText(String.format("%02d:%02d", 0, 0));
                    stopStopWatch = true;
                    try {
                        new AsFastAsYouCanScore();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    setVisible(false);
                    break;
                }
                if (minutes == 0 && seconds < 10) {
                    milisecondsLabel.setForeground(Color.red);
                    minutesAndSecondsLabel.setForeground(Color.RED);
                }
                milisecondsLabel.setText(String.format(":%03d", miliseconds));
                minutesAndSecondsLabel.setText(String.format("%02d:%02d", minutes, seconds));
            }
        };
        scheduler.scheduleAtFixedRate(runnable, 0, 1, TimeUnit.MILLISECONDS);
    }

    private void random() {
        Random random = new Random();
        if (length == 1)
            number = random.nextInt(10);
        else
            number = random.nextInt(9 * ((int) Math.pow(10, length - 1))) + (int) Math.pow(10, length - 1);
    }

    static int getPoints() {
        return points;
    }

    static void setPoints(int points) {
        AsFastAsYouCanGame.points = points;
    }

    static void setTimer(JLabel icon) {
        icon.setVisible(true);
        ActionListener taskPerformer = e -> icon.setVisible(false);
        Timer tick = new Timer(750, taskPerformer);
        tick.setRepeats(false);
        tick.start();
    }

    private void check() {
        if (textLabel.getText().equals(String.valueOf(number))) {
            incorrect.setVisible(false);
            points += length;
            score.setText(": " + points);
            textLabel.setText("");
            random();
            tmpShow.setText(String.valueOf(number));
            setTimer(correct);
        } else {
            correct.setVisible(false);
            setTimer(incorrect);
        }
    }

    private void backSpace(JLabel label) {
        if (label.getText().length() > 0)
            textLabel.setText(textLabel.getText().substring(0, textLabel.getText().length() - 1));
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(check))
            check();
        if (e.getSource().equals(backspace))
            backSpace(textLabel);
        if (e.getSource().equals(clear))
            textLabel.setText("");
    }
}